<?php
require_once 'config.php';
require_once 'restapi.php';
$logs = logErrorMessage('Starting Connection');
session_start();

$token_url = SF_LOGIN_URI . "/services/oauth2/token";

$code = $_GET['code'];

if (!isset($code) || $code == "") {
    $logs = logErrorMessage('Code Parameter Missing From Request');
    die("Error - code parameter missing from request!");
}

$params = "code=" . $code
    . "&grant_type=authorization_code"
    . "&client_id=" . SF_CLIENT_ID
    . "&client_secret=" . SF_CLIENT_SECRET
    . "&redirect_uri=" . urlencode(SF_REDIRECT_URI);

$curl = curl_init($token_url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

$json_response = curl_exec($curl);

$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ( $status != 200 ) {
    $logs = logErrorMessage("Error: call to token URL $token_url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
    die("Error: call to token URL $token_url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
}

curl_close($curl);

$response = json_decode($json_response, true);

$accessToken = $response['access_token'];
$instanceUrl = $response['instance_url'];

if (!isset($accessToken) || $accessToken == "") {
    $logs = logErrorMessage('Error Access Token Missing From Response');
    die("Error - access token missing from response!");
}

if (!isset($instanceUrl) || $instanceUrl == "") {
    $logs = logErrorMessage('Error - instance URL missing from response!');
    die("Error - instance URL missing from response!");
}

$_SESSION['accessToken'] = $accessToken;
$_SESSION['instanceUrl'] = $instanceUrl;

header('Location: daily.php');


function reconnect() {
	$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $accessToken"));

    $json_response = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($json_response, true);
    
}
?>