<?php

//  _____       _            __                   
// /  ___|     | |          / _|                  
// \ `--.  __ _| | ___  ___| |_ ___  _ __ ___ ___ 
//  `--. \/ _` | |/ _ \/ __|  _/ _ \| '__/ __/ _ \
// /\__/ / (_| | |  __/\__ \ || (_) | | | (_|  __/
// \____/ \__,_|_|\___||___/_| \___/|_|  \___\___|
//                                               
                                               
//waves
function getNewRetainers($instanceUrl, $accessToken) {
    set_time_limit(0);
	$query = "SELECT AccountId, ActivityDate, Description, Event_Type__c, Id, Retainer_Amount__c, Subject, Type, WhatId, WhoId 
    FROM Task WHERE 
        Retainer_Amount__c > 0 
            ORDER BY CreatedDate DESC NULLS LAST";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $newRetainers = getSalesforceRecords($url, $accessToken);
    return $newRetainers;
}
//AND LastModifiedDate >= LAST_N_DAYS:2
//waves
function getProspectById($instanceUrl, $accessToken, $id) {
    set_time_limit(0);
    $query = "SELECT 
        Id, Name, Primary_Email__c, Matter_Type_A__c, Matter_Type_B__c, Retained_Date__c, Retainer_Amount__c, HS_Deal_ID__c 
            FROM Propsect__c  WHERE 
                Id = '$id' 
                    LIMIT 1";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $logs = logErrorMessage('Grabbed '.$id.' record from Salesforce: '.$url);
    $prospect = getSalesforceRecords($url, $accessToken);
    return $prospect[0];
}

//waves
function getProspectEmailFromAccount($instanceUrl, $accessToken, $id) {
    set_time_limit(0);
	$query = "SELECT 
        Id, PersonEmail
            FROM Account  WHERE 
                Id = '$id' 
                    LIMIT 1";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $logs = logErrorMessage('Grabbed '.$id.' record from Salesforce: '.$url);
    $account = getSalesforceRecords($url, $accessToken);
    return $account[0]['PersonEmail'];
}

//waves
function getSalesforceRecords($url, $accessToken) {
    set_time_limit(0);
	$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ["Authorization: OAuth $accessToken"]);
    
    $jsonResponse = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($jsonResponse, true);
    if(isset($response['totalSize']) && $response['totalSize'] > 0) {
        echo $response['totalSize'].'<br>';
    	$logs = logErrorMessage('Grabbed '.$response['totalSize'].' records from Salesforce: '.$url);
        return $response['records'];
    }
    return $response;
}

//waves
function attachDealIdInSalesforce($salesforceId, $dealId, $accessToken, $instanceUrl) {
    set_time_limit(0);
    $url = "$instanceUrl/services/data/v35.0/sobjects/Propsect__c/$salesforceId";
	$postBody = json_encode(["HS_Deal_ID__c" => $dealId]);
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ["Authorization: OAuth $accessToken", "Content-type: application/json"]);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postBody);

    curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ( $status != 204 ) {
    	$error = "Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
    	$logs = logErrorMessage($error);
        die($error);
    }
    echo "HTTP status $status updating account<br/><br/>";
    curl_close($curl);
    return true;
}


//  _   _       _     _____             _   
// | | | |     | |   /  ___|           | |  
// | |_| |_   _| |__ \ `--. _ __   ___ | |_ 
// |  _  | | | | '_ \ `--. \ '_ \ / _ \| __|
// | | | | |_| | |_) /\__/ / |_) | (_) | |_ 
// \_| |_/\__,_|_.__/\____/| .__/ \___/ \__|
//                         | |              
//                         |_|              



//waves
function getHsContactVid($email, $prospect) {
	$hsContact = getHubspotContact($email, $prospect);
	if(!isset($hsContact) || is_null($hsContact)) {
		return false;
	}
	return $hsContact['vid'];
}

//waves
function createDealInHubspot($vid, array $prospect) {
	$dealExists = is_null($prospect['dealid']) ? false : getDuplicateHsDeal($vid, $prospect);
	if($dealExists) {
		$logs = logErrorMessage('Deal Already Exists In Hubspot: '.json_encode($prospect));
		return false;
	}
	$newDeal = organizeDealProperties($vid, $prospect);
	$created = saveDealInHubspot($newDeal, $prospect['sfid']);
	return $created;
}

//waves
function getDuplicateHsDeal($vid, array $prospect) {
	//because you never know
	return true;
}

//waves
function getHubspotContact($email, $prospect) {
    $url = "https://api.hubapi.com/contacts/v1/contact/email/$email/profile/?hapikey=".HS_API_KEY."&portalId=".HS_PORTAL_ID;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    
    $jsonResponse = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($jsonResponse, true);
    if(isset($response['message']) && $response['message'] = 'contact does not exist') {
    	$logs = logErrorMessage('Contact '.$email.' not valid in Hubspot. Creating Contact');
        return createHubspotContact($email, $prospect);
    }
    return $response;
}

//waves
function createHubspotContact($email, $prospect) {
    $newContact = getHsJsonContact($email, $prospect);
    $url = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.HS_API_KEY.'&portalId='.HS_PORTAL_ID;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $newContact);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    $apiResponse = curl_exec($curl);
    curl_close($curl);
	$response = json_decode($apiResponse, true);
    $logs = logErrorMessage('Created Contact In Hubspot: '.$apiResponse);
    $newContact = getHubspotContact($email, $prospect);
    return $newContact;
}

//waves
function getHsJsonContact($email, $prospect) {
    
    $name = explode(' ', $prospect['Name']);
    $json = '{
            "properties": [
                {
                    "property": "email",
                    "value": "'.$prospect['Primary_Email__c'].'"
                },
                {
                    "property": "firstname",
                    "value": "'.ucfirst($name[0]).'"
                },
                {
                    "property": "lastname",
                    "value": "'.ucfirst($name[1]).'"
                }
            ]
        }';
        return $json;
}

//waves
function saveDealInHubspot($newDeal, $salesforceId) {
	$url = 'https://api.hubapi.com/deals/v1/deal/?hapikey='.HS_API_KEY.'&portalId='.HS_PORTAL_ID;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $newDeal);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    $apiResponse = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($apiResponse, true);
    if($response) {
    	$salesforceProspectUpdate = attachDealIdInSalesforce($salesforceId, $response['dealId'], $_SESSION['accessToken'], $_SESSION['instanceUrl']);
    	return $salesforceProspectUpdate;
    }
    return $response;
}







//waves
function organizeDealProperties($vid, array $prospect) {
	$deal = '{
		"associations": {
			"associatedVids": [
				'.$vid.'
			]
		},
			"portalId": '.HS_PORTAL_ID.',
			"properties": [
				{
					"value": "'.$prospect['dealname'].'",
					"name": "dealname"
				},
				{
					"name": "description",
					"value": "'.$prospect['dealdescription'].'"
				},
				{
					"value": "closedwon",
					"name": "dealstage"
				},
				{
					"value": '.$prospect['closedate'].',
					"name": "closedate"
				},
				{
					"value": '.$prospect['amount'].',
					"name": "amount"
				},
				{
					"value": "newbusiness",
					"name": "dealtype"
				},
				{
					"value": "'.$prospect['sfid'].'",
					"name": "sf_prospect_id"
				}
			]
		}';
        return $deal;
}

//waves
function logErrorMessage($error) {
	$message = "[ ".date('Y-m-d H:i:s')." ] $error\r\n";
	file_put_contents(LOG, $message, FILE_APPEND);
}


?>



