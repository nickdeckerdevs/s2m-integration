<?php

/*  Setup Cron to run 24 hours
 *  0 0 * * * /script_directory/daily.php  
*/

require_once 'config.php';


require_once 'kint-master/Kint.class.php';
session_start();

function checkForNewRecords($instanceUrl, $accessToken) {
    // $query = "SELECT 
    //     Id, Primary_Email__c, Name, Matter_Type_A__c, Matter_Type_B__c, Retained_Date__c, Retainer_Amount__c, HS_Deal_ID__c 
    //         FROM Propsect__c  WHERE 
    //             Retainer_Amount__c > 0 AND Primary_Email__c != '' 
    //                 ORDER BY Primary_Email__c ASC";
    $query = "SELECT Id, Person__c, LastModifiedDate, HS_Deal_ID__c, Primary_Email__c, Retained_Date__c, Retainer_Amount__c 
    FROM Propsect__c WHERE 
        Retainer_Amount__c > 0 AND LastModifiedDate >= LAST_N_DAYS:1 AND HS_Deal_ID__c = null
            ORDER BY LastModifiedDate DESC";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $updated = getSalesforceRecords($url, $accessToken);
    ddd($updated);
    return $prospects;
}

function getProspectsWithRetainers($instanceUrl, $accessToken) {
	$query = "SELECT 
		Id, Primary_Email__c, Name, Matter_Type_A__c, Matter_Type_B__c, Retained_Date__c, Retainer_Amount__c, HS_Deal_ID__c 
			FROM Propsect__c  WHERE 
				Retainer_Amount__c > 0 AND Primary_Email__c != '' 
					ORDER BY Primary_Email__c ASC";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $prospects = getSalesforceRecords($url, $accessToken);
    return $prospects;
}

function getEventWithProspect($instanceUrl, $accessToken) {
    $query = "SELECT AccountId, ActivityDate, Description, Event_Type__c, Id, Retainer_Amount__c, Subject, Type, WhatId, WhoId 
    FROM Task WHERE 
        Retainer_Amount__c = 2000 AND ActivityDate = 2015-12-10 
            ORDER BY CreatedDate DESC NULLS LAST";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $events = getSalesforceRecords($url, $accessToken);
    return $events;

}

function getTestProspect($instanceUrl, $accessToken, $email) {
    $query = "SELECT 
        Id, Primary_Email__c, Matter_Type_A__c, Matter_Type_B__c, Retained_Date__c, Retainer_Amount__c, HS_Deal_ID__c 
            FROM Propsect__c  WHERE 
                Retainer_Amount__c > 0 AND Primary_Email__c = '$email' 
                    ORDER BY Primary_Email__c ASC";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $prospects = getSalesforceRecords($url, $accessToken);
    return $prospects;
}

function getProspectById($instanceUrl, $accessToken, $id) {
    $query = "SELECT 
        Id, Name, Primary_Email__c, Matter_Type_A__c, Matter_Type_B__c, Retained_Date__c, Retainer_Amount__c, HS_Deal_ID__c 
            FROM Propsect__c  WHERE 
                Id = '$id' 
                    LIMIT 1";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $prospect = getSalesforceRecords($url, $accessToken);
    return $prospect[0];
}

function getProspectByPerson($instanceUrl, $accessToken, $id) {
    $query = "SELECT 
        Id, Name, Person, Primary_Email__c, Matter_Type_A__c, Matter_Type_B__c, Retained_Date__c, Retainer_Amount__c, HS_Deal_ID__c 
            FROM Propsect__c  WHERE 
                Id = '$id' 
                    LIMIT 1";
    $url = "$instanceUrl/services/data/v35.0/query?q=" . urlencode($query);
    $prospect = getSalesforceRecords($url, $accessToken);
    return $prospect[0];
}

function getSalesforceRecords($url, $accessToken) {
	$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ["Authorization: OAuth $accessToken"]);
    
    $jsonResponse = curl_exec($curl);
    d($jsonResponse);
    curl_close($curl);

    $response = json_decode($jsonResponse, true);
    d($response);
    if(isset($response['totalSize']) && $response['totalSize'] > 0) {
        return $response['records'];
    }
    // echo 'Total Records: ' . $response['totalSize'] . '<br>';
    return $response;
}

function updateSalesforceRecords($id, $data) {
    $access_token = $_SESSION['access_token'];
    $instance_url = $_SESSION['instance_url'];
    $url = $instance_url.'/services/data/v34.0/sobjects/Prospect__c/'.$id;

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ["Authorization: OAuth $access_token", "Content-type: application/json"]);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    $tomfuckery = curl_exec($curl);
    d($tomfuckery);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    ddd($status);
    curl_close($curl);

    $response = json_decode($jsonResponse, true);
    // echo 'Total Records: ' . $response['totalSize'] . '<br>';
    return $response['records'];
}

// function fuckThis($id, $instance_url, $access_token) {
//     $url = "$instance_url/services/data/v35.0/sobjects/Prospect__c/Id/$id";

//     $curl = curl_init($url);
//     curl_setopt($curl, CURLOPT_HEADER, false);
//     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//     curl_setopt($curl, CURLOPT_HTTPHEADER,
//             array("Authorization: OAuth $access_token"));

//     $json_response = curl_exec($curl);

//     $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

//     if ( $status != 200 ) {
//         die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
//     }

//     echo "HTTP status $status reading account<br/><br/>";

//     curl_close($curl);

//     $response = json_decode($json_response, true);

//     foreach ((array) $response as $key => $value) {
//         echo "$key:$value<br/>";
//     }
//     echo "<br/>";
//     ddd('shit is shown');
// }

    function show_account($id, $instance_url, $access_token) {
        $url = "$instance_url/services/data/v35.0/sobjects/Propsect__c/$id";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                array("Authorization: OAuth $access_token"));

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status != 200 ) {
            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }

        echo "HTTP status $status reading account<br/><br/>";

        curl_close($curl);

        $response = json_decode($json_response, true);

        foreach ((array) $response as $key => $value) {
            echo "$key:$value<br/>";
        }
        echo "<br/>";
        // ddd('got the shit');
        return $response;

    }

function update_account($id, $new_name, $city, $instance_url, $access_token) {
    $url = "$instance_url/services/data/v35.0/sobjects/Propsect__c/$id";

    $content = json_encode(["HS_Deal_ID__c" => $new_name]);

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $access_token",
                "Content-type: application/json"));
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

    curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    d($status);
    if ( $status != 204 ) {
        die("Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
    }

    echo "HTTP status $status updating account<br/><br/>";

    curl_close($curl);
}

function attachDealIdInSalesforce($salesforceId, $dealId, $accessToken, $instanceUrl) {
    $accessToken = $_SESSION['access_token'];
    $instanceUrl = $_SESSION['instance_url'];
    $url = "$instanceUrl/services/data/v35.0/sobjects/Propsect__c/$salesforceId";

    $postBody = json_encode(["HS_Deal_ID__c" => $dealId]);
    d($postBody);
    d($url);
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $accessToken",
                "Content-type: application/json"));
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postBody);

    curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    d($status);
    if ( $status != 204 ) {
        die("Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
    }
    echo "HTTP status $status updating account<br/><br/>";
    curl_close($curl);
    ddd('shits');
    return $response;
}

function getHsContactVid($email, $prospect) {
	$hsContact = getHubspotContact($email, $prospect);
	if(!isset($hsContact) || is_null($hsContact)) {
		return false;
	}
	return $hsContact['vid'];
}

function createDealInHubspot($vid, array $prospect) {
    // ddd($prospect);
	$dealExists = is_null($prospect['dealid']) ? false : getDuplicateHsDeal($vid, $prospect);
	d($dealExists);
	if($dealExists) {
		$logs = logErrorMessage('Deal Already Exists In Hubspot: '.json_encode($prospect));
		//update deal
		return false;
	}
	$newDeal = organizeDealProperties($vid, $prospect);
	d($newDeal);
	$created = saveDealInHubspot($newDeal, $prospect['sfid']);
	d($created);
	return $created;
}

function getDuplicateHsDeal($vid, array $prospect) {
	return true;
}

function getHubspotContact($email, $prospect) {
    $url = "https://api.hubapi.com/contacts/v1/contact/email/$email/profile/?hapikey=".HS_API_KEY."&portalId=".HS_PORTAL_ID;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    
    $jsonResponse = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($jsonResponse, true);
    if(isset($response['message']) && $response['message'] = 'contact does not exist') {
        return createHubspotContact($email, $prospect);
    }
    return $response;
}

function createHubspotContact($email, $prospect) {
    $newContact = getHsJsonContact($email, $prospect);
    $url = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='.HS_API_KEY.'&portalId='.HS_PORTAL_ID;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $newContact);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    $apiResponse = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($apiResponse, true);
    $newContact = getHubspotContact($email, $prospect);
    return $newContact;
}

function getHsJsonContact($email, $prospect) {
    
    $name = explode(' ', $prospect['Name']);
    $json = '{
            "properties": [
                {
                    "property": "email",
                    "value": "'.$prospect['Primary_Email__c'].'"
                },
                {
                    "property": "firstname",
                    "value": "'.ucfirst($name[0]).'"
                },
                {
                    "property": "lastname",
                    "value": "'.ucfirst($name[1]).'"
                }
            ]
        }';
        return $json;
}

function saveDealInHubspot($newDeal, $salesforceId) {
	$url = 'https://api.hubapi.com/deals/v1/deal/?hapikey='.HS_API_KEY.'&portalId='.HS_PORTAL_ID;
    $curl = curl_init($url);
    // ddd($url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $newDeal);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    $apiResponse = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($apiResponse, true);
    if($response) {
    	$salesforceProspectUpdate = attachDealIdInSalesforce($salesforceId, $response['dealId'], $_SESSION['access_token'], $_SESSION['instance_url']);
    }
    return $response;
}

function getHubspotDeals() {

}

function organizeDealProperties($vid, array $prospect) {
	$deal = '{
		"associations": {
			"associatedVids": [
				'.$vid.'
			]
		},
			"portalId": '.HS_PORTAL_ID.',
			"properties": [
				{
					"value": "'.$prospect['dealname'].'",
					"name": "dealname"
				},
				{
					"name": "description",
					"value": "'.$prospect['dealdescription'].'"
				},
				{
					"value": "closedwon",
					"name": "dealstage"
				},
				{
					"value": '.$prospect['closedate'].',
					"name": "closedate"
				},
				{
					"value": '.$prospect['amount'].',
					"name": "amount"
				},
				{
					"value": "newbusiness",
					"name": "dealtype"
				},
				{
					"value": "'.$prospect['sfid'].'",
					"name": "sf_prospect_id"
				}
			]
		}';
        return $deal;
}

function logErrorMessage($error) {
	$message = "[ ".date('Y-m-d H:i:s')." ] $error\r\n";
	file_put_contents(LOG, $message, FILE_APPEND);
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>{ [ WLG Salesforce Export => Hubspot Import ] => [ Retainer => Deals ] } </title>
    </head>
    <body>
        <tt>
            <?php

            $access_token = $_SESSION['access_token'];
            $instance_url = $_SESSION['instance_url'];
            
            if (!isset($access_token) || $access_token == "") {
                die("Error - access token missing from session!");
            }

            if (!isset($instance_url) || $instance_url == "") {
                die("Error - instance URL missing from session!");
            }

            // 24 hours

            $newRecords = checkForNewRecords($instance_url, $access_token);

            //
            //
            //   UPDATE
            //
            //
            //

            $records = getEventWithProspect($instance_url, $access_token);
            foreach($records as $record) {
                $prospect = getProspectById($instance_url, $access_token, $record['WhatId']);
                $email = $prospect['Primary_Email__c'];
                $vid = getHsContactVid($email, $prospect);
                $dealProperties = [
                    'sfid' => $record['WhatId'],
                    'email' => $email,
                    'dealname' => $prospect['Matter_Type_A__c'],
                    'dealdescription' => $prospect['Matter_Type_B__c'],
                    'amount' => $prospect['Retainer_Amount__c'],
                    'closedate' => strtotime($prospect['Retained_Date__c']),
                    'dealid' => $prospect['HS_Deal_ID__c']
                ];

                $deal = createDealInHubspot($vid, $dealProperties);
                if($deal == false) {
                    
                }
                ddd($deal);
                //add deal id in SF
            }
            ddd('THATS EVENTS');

            //manually creating the deal instead of getting prospect record from SF

            // $vid = 51444;
            // $dealname = 'Deal Returning Deal Id';
            // $dealdescription = 'This deal ID will be registered in Salesforce for future updating';
            // $customDate = strtotime('2016-01-23');
            // $amount = 1738;
            // $dealProperties = [
            // 	'sfid' => '00Qj000000IwsA9EAJ',
            // 	'email' => 'ndecker@square2marketing.com',
            // 	'dealname' => $dealname,
            // 	'dealdescription' => $dealdescription,
            // 	'amount' => $amount,
            // 	'closedate' => $customDate,
            // 	'dealid' => null
            // ];

            //sending the deal to HS

            
            





/*

            $prospects = getProspectsWithRetainers($instanceUrl, $accessToken);
            
            foreach($prospects as $prospect) {
            	$email = $prospect['Primary_Email__c'];
            	$vid = getHsContactVid($email);
            	if($vid == false) { 
            		$logs = logMessage('Contact Not Found In Hubspot with Email Address: '.$email);
        			continue;
            	}
            	$hsDeal = createDealInHubspot($vid, $prospect);

            }


*/
            
            ?>
        </tt>
    </body>
</html>


