<?php

/*  Setup Cron to run 24 hours
 *  0 0 * * * /script_directory/daily.php  
*/
set_time_limit(0);
require_once 'config.php';
require_once 'restapi.php';
require_once 'kint-master/Kint.class.php';
session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>{ [ WLG Salesforce Export => Hubspot Import ] => [ Retainer => Deals ] } </title>
    </head>
    <body>
        <tt>
            <?php
            $startMessage = 'API SESSION START';
            var_dump($startMessage);
            $logs = logErrorMessage($startMessage);
            $accessToken = $_SESSION['accessToken'];
            $instanceUrl = $_SESSION['instanceUrl'];
            
            if (!isset($accessToken) || $accessToken == "") {
                $logs = logErrorMessage('Access Token Missing');
                die("Error - access token missing from session!");
            }

            if (!isset($instanceUrl) || $instanceUrl == "") {
                $logs = logErrorMessage('Access Token Missing');
                die("Error - instance URL missing from session!");
            }

            $records = getNewRetainers($instanceUrl, $accessToken);
            foreach($records as $record) {
                $sfid = $record['WhatId'];
                $prospect = getProspectById($instanceUrl, $accessToken, $sfid);
                $email = is_null($prospect['Primary_Email__c']) ? getProspectEmailFromAccount($instanceUrl, $accessToken, $record['AccountId']) : $prospect['Primary_Email__c'];
                $vid = getHsContactVid($email, $prospect);
                $dealProperties = [
                    'sfid' => $sfid,
                    'email' => $email,
                    'dealname' => $prospect['Matter_Type_A__c'],
                    'dealdescription' => $prospect['Matter_Type_B__c'],
                    'amount' => $prospect['Retainer_Amount__c'],
                    'closedate' => strtotime($prospect['Retained_Date__c']),
                    'dealid' => $prospect['HS_Deal_ID__c']
                ];

                $deal = createDealInHubspot($vid, $dealProperties);
                if($deal == false) {
                    echo 'deal not logged';
                    $logs = logErrorMessage('Deal Not Logged for '.json_encode($dealProperties));
                }
            }

            
            ?>
        </tt>
    </body>
</html>

